from django.conf.urls import patterns, url
from weekopgave_1_opgave_5.sessieview import sessie_form_get, sessie_form_post

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    url(r'^sessie_form_get$', sessie_form_get),
    url(r'^sessie_form_post$', sessie_form_post),
    # Examples:
    # url(r'^$', 'weekopgave_1_opgave_5.views.home', name='home'),
    # url(r'^weekopgave_1_opgave_5/', include('weekopgave_1_opgave_5.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
